#include <SPI.h>  // SPI
#include <Wire.h> // Wire
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <Adafruit_BMP085.h> // BMP - Barometic sensor library
#include <NewPing.h> // NewPing 

// the display pins in play - If you arent using a Mega2560 for this, you should be...
#define sclk 23  // BLUE wire
#define mosi 25  // GREEN wire
#define dc   27  // WHITE wire
#define rst  29  // ORANGE wire
#define cs   31  // YELLOW wire

int maxdistance=150;
// Ultrasonic-01
int echoPINGright=50;
int triggerPINGright=52; // ping return signal pin
unsigned long triggerVALright=0;
NewPing sonarRight(triggerPINGright,echoPINGright,maxdistance); // NewPing setup of pins and maximum distance.

// Ultrasonic-02
unsigned long echoPINGleft = 24;
unsigned long triggerPINGleft = 22; // Ultrasound signal pin
unsigned long triggerVALleft=0;
NewPing sonarLeft(triggerPINGleft,echoPINGleft,maxdistance); // NewPing setup of pins and maximum distance.

// 3-axis accellerometer uses analog pins
const int xpin = A0;                  // x-axis of the accelerometer
const int ypin = A1;                  // y-axis
const int zpin = A2;                  // z-axis (only on 3-axis models)
const int maxSonarpin = A3; // MaxBotix sonar - EZ

Adafruit_ST7735 tft = Adafruit_ST7735(cs, dc, mosi, sclk, rst);
// Color definitions  These may not be the same colors from the adafruit tutorials (off brand display & LSB/MSB issue?)
    #define BLACK 0x0000
    #define RED 0x001F
    #define REDdk2 0x001A
    #define REDdk 0x000A
    #define BLUE 0xF800
    #define BLUEdk1 0x4101
    #define BLUEGREYdk 0x2101
    #define GREEN 0x07E0
    #define GREENdk 0x0100
    #define OLIVE 0x0460
    #define YELLOW 0x07FF
    #define MAGENTA 0xF81F
    #define CYAN 0xFFE0
    #define WHITE 0xFFFF
 
// theme colors.   The icons will reference these for outlione, fill and transparency 

// int myTheme [] = {iconTRAN,iconCOLR,iconFILL,iconALRM,SENSORGRID};
//int myTheme [] = {CYAN,YELLOW,REDdk,RED,dkRED};
//int myTheme [] = {CYAN,GREEN,OLIVE,YELLOW,OLIVE}; // Greens - kinda bright
//int myTheme [] = {CYAN,OLIVE,GREENdk,YELLOW,GREENdk}; // Darker Green
int myTheme [] = {CYAN,REDdk2,REDdk,YELLOW,REDdk}; // a nice red theme for nighttime
//int myTheme [] = {CYAN,BLUE,BLUEdk1,YELLOW,BLUEGREYdk}; // Da Blues

int iconTRAN=myTheme[0]; //cuz it clashes with my intended color scheme - don't draw a pixel - transparent background
int iconCOLR=myTheme[1]; // foreground color
int iconFILL=myTheme[2]; // background color
int iconALRM=myTheme[3]; // 
int SENSORGRID=myTheme[4];


int foo=0;
int myInnerDelay=0;
int mydelay=myInnerDelay;
int myOuterDelay=100;

Adafruit_BMP085 bmp;

float RoboShowVer = .04;
void setup()
{
  //
    Serial.begin(115200); // Open serial monitor at 115200 baud to see ping results.
    delay(myInnerDelay);
    tft.setTextColor(iconCOLR,iconFILL);
    tft.setTextSize(0);
    tft.initR(INITR_REDTAB);   // initialize a ST7735R chip, red tab
    tft.fillScreen(BLACK);
//  initRoboShow(COLOR,float,wait time in sec);  
    initRoboShow(SENSORGRID,RoboShowVer,5);
     //tft.initR(INITR_REDTAB);   // initialize a ST7735R chip, red tab
    //tft.fillScreen(BLACK);
}
 
 

void loop()
{

  // in general:  foo(x,y,sensorval);
//  putPIR(25,45,23);
//  putPIR(105,45,2);
//  putPING(35,60, 200);
//  putPING(75,60, 200);
  
  unsigned int uSLeft = sonarLeft.ping();
  unsigned int uSRight = sonarRight.ping();
  iconPIRshow(10,35,0); // x,y,value
  iconPIRshow(105,35,0); // x,y,value
  iconPINGshow(35,60,(uSLeft/US_ROUNDTRIP_CM)); // x,y,value
  iconPINGshow(75,60,(uSRight/US_ROUNDTRIP_CM)); // you get the idea
  putBMPalt(35,90,'F'); // 'F' for Farenheight, 'C' for Celsius
  putMAXsonar(5,100);
  putBMPtemp(5,120,'F'); // 'F' for Feet, 'M' for Meters
  putACCEL(60,120);
  putBMPbar(5,140); 
  iconBATTshow(118,145,50); // bottom right of 1.8"tft
}
//
// PIR Display
//
int iconPIRh=12;
int iconPIRw=16;
// const byte icon [h] [w]
const int iconPIR [12] [16] = {
{iconCOLR,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconCOLR},
{iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconCOLR,iconTRAN},
{iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN},
{iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconTRAN},
{iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconTRAN,iconTRAN},
{iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN},
{iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN},
{iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN},
{iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconCOLR,iconTRAN,iconTRAN,iconTRAN,iconTRAN},
{iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconCOLR,iconTRAN,iconTRAN,iconTRAN},
{iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconCOLR,iconTRAN,iconTRAN,iconTRAN},
{iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconTRAN,iconTRAN,iconTRAN},
};
// read the colors from the array and draw the pixel
void iconPIRshow(int x, int y,int sensorval){
  int rfoo=0;
  int cfoo=0;
  int r,c;
  for (rfoo=0;rfoo<iconPIRh;)
  {
    for (cfoo=0;cfoo<iconPIRw;)
    {
     if (iconPIR[rfoo][cfoo]!=iconTRAN) {
       tft.drawPixel(x+cfoo,y+rfoo,iconPIR[rfoo][cfoo]);
     }
     cfoo++;
    }  
    rfoo++;
  }
  tft.setTextColor(iconCOLR,iconFILL);
  tft.setTextSize(0);
  tft.fillRect(x, y+rfoo+2, cfoo, 11, iconFILL);
  tft.drawRect(x, y+rfoo+2, cfoo, 11, iconCOLR);
  tft.setCursor(x+3, y+rfoo+4);
  tft.print(sensorval);
}
//
// PING Display
//
int iconPINGh=10;
int iconPINGw=21;
// const byte icon [h] [w]
const int iconPING [10] [21] = {
//FLASH_TABLE(byte, iconPING, 21, 
{iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconTRAN},
{iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconTRAN,iconTRAN},
{iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconCOLR,iconTRAN,iconTRAN,iconTRAN,iconTRAN},
{iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconCOLR,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconCOLR,iconCOLR,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN},
{iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN,iconTRAN},
{iconTRAN,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconTRAN},
{iconCOLR,iconCOLR,iconFILL,iconFILL,iconCOLR,iconFILL,iconFILL,iconCOLR,iconFILL,iconFILL,iconCOLR,iconFILL,iconFILL,iconCOLR,iconFILL,iconFILL,iconCOLR,iconFILL,iconFILL,iconCOLR,iconCOLR},
{iconCOLR,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconCOLR},
{iconCOLR,iconCOLR,iconFILL,iconFILL,iconCOLR,iconFILL,iconFILL,iconCOLR,iconFILL,iconFILL,iconCOLR,iconFILL,iconFILL,iconCOLR,iconFILL,iconFILL,iconCOLR,iconFILL,iconFILL,iconCOLR,iconCOLR},
{iconTRAN,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconTRAN}
};
//);
// read the colors from the array and draw the pixel
void iconPINGshow(int x, int y,int sensorval){
  int rfoo=0;
  int cfoo=0;
  int r,c;
  for (rfoo=0;rfoo<iconPINGh;)
  {
    for (cfoo=0;cfoo<iconPINGw;)
    {
     if (iconPING[rfoo][cfoo]!=iconTRAN) {
       int myPixel=iconPING[rfoo][cfoo];
       tft.drawPixel(x+cfoo,y+rfoo,myPixel);
     }
     cfoo++;
    }  
    rfoo++;
  }
  tft.setTextColor(iconCOLR,iconFILL);
  tft.setTextSize(0);
  tft.fillRect(x-1, y+rfoo+2, cfoo+2, 11, iconFILL);
  tft.drawRect(x-1, y+rfoo+2, cfoo+2, 11, iconCOLR);
  tft.setCursor(x+2, y+rfoo+4);
  tft.print(sensorval);
}
//
// BATTERY Display
//
int iconBATTh=12;
int iconBATTw=7;
// const byte icon [h] [w]
const int iconBATT [12] [7] = {
{iconTRAN,iconTRAN,iconCOLR,iconCOLR,iconCOLR,iconTRAN,iconTRAN},
{iconTRAN,iconTRAN,iconCOLR,iconFILL,iconCOLR,iconTRAN,iconTRAN},
{iconTRAN,iconCOLR,iconFILL,iconFILL,iconFILL,iconCOLR,iconTRAN},
{iconCOLR,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconCOLR},
{iconCOLR,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconCOLR},
{iconCOLR,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconCOLR},
{iconCOLR,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconCOLR},
{iconCOLR,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconCOLR},
{iconCOLR,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconCOLR},
{iconCOLR,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconCOLR},
{iconCOLR,iconFILL,iconFILL,iconFILL,iconFILL,iconFILL,iconCOLR},
{iconTRAN,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconCOLR,iconTRAN},
};
// read the colors from the array and draw the pixel
void iconBATTshow(int x, int y,int sensorval){
  int rfoo=0;
  int cfoo=0;
  int r,c;
  for (rfoo=0;rfoo<iconBATTh;)
  {
    for (cfoo=0;cfoo<iconBATTw;)
    {
     if (iconBATT[rfoo][cfoo]!=iconTRAN) {
       tft.drawPixel(x+cfoo,y+rfoo,iconBATT[rfoo][cfoo]);
     }
     cfoo++;
    }  
    rfoo++;
  }
  tft.setTextSize(0);
  tft.setCursor(x-15, y+3);
  tft.print(sensorval);
}
//
// PING sensor display - linedraw method
//
//void putPING(int x,int y, int sensorval) {
//  tft.setTextSize(0);
//  tft.setCursor(x+2, y+2);
//  tft.fillRect(x, y, 22, 12, iconFILL);
//  tft.drawRect(x, y, 22, 12, iconCOLR);
//  tft.print(sensorval);
//}
//
// PIR sensor display - LineDraw method
//
//void putPIR(int x,int y, int sensorval) {
//  int raysize=15;
//  tft.setTextSize(0);
//  tft.setCursor(x-4, y-13);
//  tft.fillTriangle(x, y, x-raysize , y-raysize ,x+raysize ,y-raysize, iconFILL);
//  tft.drawTriangle(x, y, x-raysize , y-raysize ,x+raysize ,y-raysize, iconCOLR);
//  tft.print(sensorval);
//}
//
// BMP - Barometer, Temp, Altimeter
//
void putBMPtemp(int x,int y,char myUnit) {
    tft.setTextColor(iconCOLR,iconFILL);
    tft.setTextSize(0);
    if (!bmp.begin()) {
	Serial.println("Could not find a valid BMP085 sensor, check wiring!");
	while (1) {}
    }
    float tc=bmp.readTemperature();
    float tf=tc*9/5+32;
    tft.fillRect(x, y, 50, 11, iconFILL);
    tft.drawRect(x, y, 50, 11, iconCOLR);
    tft.setCursor(x+3, y+2);
    if (myUnit=='F') {
      tft.print(tf);
      tft.println("F");    
    } else {
      tft.print(tc);
      tft.println("C");
    }
}
//
// Display Altimeter data
void putBMPalt(int x,int y,char myUnit) {
    tft.setTextColor(iconCOLR,iconFILL);
    tft.setTextSize(0);
    tft.setCursor(x, y);
    if (!bmp.begin()) {
	Serial.println("Could not find a valid BMP085 sensor, check wiring!");
	while (1) {}
    }
    float am=bmp.readAltitude();
    float af=am*3.280839895;
    float amr=bmp.readAltitude(101500);
    float afr=amr*3.280839895;
    tft.fillRect(x, y, 70, 11, iconFILL);
    tft.drawRect(x, y, 70, 11, iconCOLR);
    tft.setCursor(x+3, y+2);
    if (myUnit=='F') {
      tft.print(afr);
      tft.println("ft");
    } else {
      tft.print(amr);
      tft.println("m");
    }
}

void putACCEL(int x,int y) {
  tft.setTextColor(iconCOLR,iconFILL);
  tft.setTextSize(0);
  tft.setCursor(x, y);
  tft.fillRect(x, y, 35, 32, iconFILL);
  tft.drawRect(x, y, 35, 32, iconCOLR);
  tft.setCursor(x+3, y+2);
  tft.print("x:");
  tft.print(analogRead(xpin));
  tft.setCursor(x+3, y+12);
  tft.print("y:");
  tft.print(analogRead(ypin));
  tft.setCursor(x+3, y+22);
  tft.print("z:");
  tft.print(analogRead(zpin));
}




void putMAXsonar(int x,int y) {
    int sensoravg=6;
    int distsensor=0; // maxbotix sonar value initialize
    int i;
    for (i=0; i<sensoravg; i++) {
     distsensor += analogRead(maxSonarpin);
     // delay(50);
    }
  distsensor /= sensoravg;
  tft.setTextColor(iconCOLR,iconFILL);
  tft.setTextSize(0);
  tft.setCursor(x, y);
  tft.fillRect(x, y, 25, 12, iconFILL);
  tft.drawRect(x, y, 25, 12, iconCOLR);
  tft.setCursor(x+3, y+2);
  tft.print(distsensor);
  tft.print("cm");
}


void putBMPbar(int x,int y) {
    tft.setTextColor(iconCOLR,iconFILL);
    tft.setTextSize(0);
    tft.setCursor(x, y);
    if (!bmp.begin()) {
	Serial.println("Could not find a valid BMP085 sensor, check wiring!");
	while (1) {}
    }
    float pa=bmp.readPressure();
    tft.fillRect(x, y, 50, 11, iconFILL);
    tft.drawRect(x, y, 50, 11, iconCOLR);
    tft.setCursor(x+3, y+2);
      tft.print(pa,0);
      tft.println("pa");
}
//
//
//


//
// Show the RoboShow ver# and init the grid.
//
void initRoboShow(uint16_t color,float ver,int waitforsensors) {
  tft.fillScreen(BLACK);
  putGrid(SENSORGRID);
  tft.setCursor(23,3);
  tft.setTextColor(YELLOW);
  tft.print("RoboShow v"); 
  tft.print(RoboShowVer);
  if (waitforsensors>0) {
    tft.setCursor(3,100);
    tft.print("... initializing ...");
    tft.setCursor(3,110);
    tft.print(  "...    sensors   ...");
    tft.fillRect(35, 130, 60, 12, iconFILL);
    tft.drawRect(35, 130, 60, 12, iconCOLR);
    tft.setCursor(40,132);
    // insert a proper counter
    tft.print("cntr:");
    tft.print((waitforsensors/1000));
    tft.print("s");
    delay(waitforsensors*1000); 
    tft.fillScreen(BLACK);
    putGrid(SENSORGRID);
  }
  tft.setCursor(23,3);
  tft.setTextColor(YELLOW);
  tft.print("RoboShow v"); 
  tft.print(RoboShowVer);
}
//
// the grid for the background
//
void putGrid(int gridcolor) {
  for (int16_t y=0; y < tft.height(); y+=6) {
    tft.drawFastHLine(0, y, tft.width(), gridcolor);
  }
  for (int16_t x=0; x < tft.width(); x+=6) {
    tft.drawFastVLine(x, 0, tft.height(), gridcolor);
  }
}

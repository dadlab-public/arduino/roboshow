# RoboShow README.md

This project was intended to display the status of various systems from my old RoboThingus project.

On a 1.8" TFT color display, the following items are displayed and regularly updated:
  // PIR - Passive InfraRed
  iconPIRshow(10,35,0); // x,y,value
  iconPIRshow(105,35,0); // x,y,value
  //
  iconPINGshow(35,60,(uSLeft/US_ROUNDTRIP_CM)); // x,y,value
  iconPINGshow(75,60,(uSRight/US_ROUNDTRIP_CM)); // you get the idea
  // Maxbotic sonar
  putMAXsonar(5,100);  
  // BaroMetricPressue - Altitude/Temp/Pressure
  putBMPalt(35,90,'F'); // 'F' for Feet, 'M' for Meters
  putBMPtemp(5,120,'F'); // 'F' for Farenheight, 'C' for Celsius
  putBMPbar(5,140);
  // 3-axis accellerometer
  putACCEL(60,120);
  // battery status (not fully integrated)
  iconBATTshow(118,145,50); // bottom right of 1.8"tft
